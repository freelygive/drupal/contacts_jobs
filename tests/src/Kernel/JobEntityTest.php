<?php

namespace Drupal\Tests\contacts_jobs\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the new entity API for the test field type.
 *
 * @group field
 */
class JobEntityTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'views',
    'contacts_jobs',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['contacts_jobs']);
    $this->installEntitySchema('view');
    $this->installEntitySchema('contacts_job');
    $this->installSchema('system', 'sequences');
  }

  /**
   * Tests using entity fields of the test field type.
   */
  public function testTestItem() {
    $time = \Drupal::time()->getRequestTime();
    $time_in_a_week = $time + 604800;
    $values = [
      'type' => 'contacts_job',
    ];

    /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
    $job = \Drupal::entityTypeManager()->getStorage('contacts_job')->create($values);
    $job->save();

    $this->assertEquals(0, $job->getClosingTime());

    $job->setClosingTime($time_in_a_week);
    $job->save();

    $this->assertEquals($time_in_a_week, $job->getClosingTime());

    $this->assertFalse($job->isPublished());
    $this->assertEquals(0, $job->getPublishStartTime());
    $this->assertEquals(0, $job->getPublishEndTime());

    $job->setPublishStartTime($time);
    $job->save();

    $this->assertTrue($job->isPublished());
    $this->assertEquals($time, $job->getPublishStartTime());
    $this->assertEquals($time_in_a_week, $job->getPublishEndTime());

    $this->assertFalse($job->isPromoted());
    $this->assertEquals(0, $job->getPromotedStartTime());
    $this->assertEquals(0, $job->getPromotedEndTime());

    $job->setPromotedStartTime($time);
    $job->save();

    $this->assertTrue($job->isPromoted());
    $this->assertEquals($time, $job->getPromotedStartTime());
    $this->assertEquals($time_in_a_week, $job->getPromotedEndTime());
  }

}
