<?php

/**
 * @file
 * Contains contacts_jobs.module.
 */

use Drupal\contacts_jobs\Entity\JobType;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_help().
 */
function contacts_jobs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the contacts_jobs module.
    case 'help.page.contacts_jobs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p></p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function contacts_jobs_theme(): array {
  $theme = [];
  $theme['contacts_job'] = [
    'render element' => 'elements',
    'file' => 'contacts_job.page.inc',
    'template' => 'contacts_job',
  ];
  $theme['contacts_job_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'contacts_job.page.inc',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function contacts_jobs_theme_suggestions_contacts_job(array $variables): array {
  $suggestions = [];
  $entity = $variables['elements']['#contacts_job'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'contacts_job__' . $sanitized_view_mode;
  $suggestions[] = 'contacts_job__' . $entity->bundle();
  $suggestions[] = 'contacts_job__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'contacts_job__' . $entity->id();
  $suggestions[] = 'contacts_job__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_entity_extra_field_info().
 */
function contacts_jobs_entity_extra_field_info(): array {
  // Define an organisation_logo field that traverses the entity reference
  // from the job to the organisation. Allows the logo to be used directly
  // in entity view displays.
  $extra = [];
  foreach (JobType::loadMultiple() as $bundle) {
    /** @var \Drupal\contacts_jobs\Entity\JobType $bundle */
    $extra['contacts_job'][$bundle->id()]['display']['organisation_logo'] = [
      'label' => new TranslatableMarkup('Organisation logo'),
      'visible' => FALSE,
    ];

    $extra['contacts_job'][$bundle->id()]['display']['organisation_address'] = [
      'label' => new TranslatableMarkup('Organisation address'),
      'visible' => FALSE,
    ];

    $extra['contacts_job'][$bundle->id()]['display']['organisation_link'] = [
      'label' => new TranslatableMarkup('Organisation link'),
      'visible' => FALSE,
    ];

    $extra['contacts_job'][$bundle->id()]['display']['links'] = [
      'label' => new TranslatableMarkup('Links'),
      'visible' => TRUE,
      'weight' => 99,
    ];
  }
  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view() for contacts_job.
 */
function contacts_jobs_contacts_job_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode): void {
  /** @var \Drupal\contacts_jobs\Entity\Job $entity */
  // Display a warning if withdrawn.
  if ($view_mode == 'full' && $entity->isWithdrawn()) {
    \Drupal::messenger()->addWarning(new TranslatableMarkup('This job has been withdrawn.'));
  }
}

/**
 * Implements hook_preprocess_page_title().
 */
function contacts_jobs_preprocess_page_title(&$variables): void {
  $job_form_routes = [
    'entity.contacts_job.post_form',
    'entity.contacts_job.add_form',
  ];
  if (in_array(Drupal::routeMatch()->getRouteName(), $job_form_routes)) {
    $variables['title'] = t('Add a job vacancy');
  }
}
