<?php

/**
 * @file
 * Contains contacts_job.page.inc.
 *
 * Page callback for Job entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Job templates.
 *
 * Default template: contacts_job.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_contacts_job(array &$variables): void {
  // Fetch Job Entity Object.
  $contacts_job = $variables['elements']['#contacts_job'];

  $variables['url'] = $contacts_job->toUrl()->toString();
  $variables['view_mode'] = $variables['elements']['#view_mode'];

  if ($contacts_job->isPromoted()) {
    $variables['attributes']['class'][] = 'promoted';
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
