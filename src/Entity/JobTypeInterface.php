<?php

namespace Drupal\contacts_jobs\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Job type entities.
 */
interface JobTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
