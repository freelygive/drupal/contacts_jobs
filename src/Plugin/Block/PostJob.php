<?php

namespace Drupal\contacts_jobs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides 'Post a job' block.
 *
 * @Block(
 *   id = "post_job",
 *   admin_label = @Translation("Post job"),
 *   category = @Translation("Contacts")
 * )
 */
class PostJob extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * A path validator service.
   *
   * @var \Drupal\Core\Path\PathValidator
   *   A path validator service.
   */
  protected PathValidator $pathValidator;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Path\PathValidator $path_validator
   *   A path validator service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PathValidator $path_validator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'button_text' => '',
      'button_link' => '',
      'message' => ['value' => NULL, 'format' => NULL],
      'error' => ['value' => NULL, 'format' => NULL],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['button_text'] = [
      '#title' => $this->t('Button text'),
      '#description' => $this->t('The text to be displayed on the button'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['button_text'],
    ];

    $form['button_link'] = [
      '#title' => $this->t('Button link'),
      '#description' => $this->t('The path to send the post job form. Access to the page will be checked.'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['button_link'],
    ];

    $form['message'] = [
      '#title' => $this->t('Message'),
      '#description' => $this->t('The visible text after the Post Job link for visitors who can post jobs.'),
      '#type' => 'text_format',
      '#default_value' => $this->configuration['message']['value'],
      '#format' => $this->configuration['message']['format'],
    ];

    $form['error'] = [
      '#title' => $this->t('Error message'),
      '#description' => $this->t('The visible text for users who cannot post jobs.'),
      '#type' => 'text_format',
      '#default_value' => $this->configuration['error']['value'],
      '#format' => $this->configuration['error']['format'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['button_link'] = $form_state->getValue('button_link');
    $this->configuration['message'] = $form_state->getValue('message');
    $this->configuration['error'] = $form_state->getValue('error');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [];

    // Checking create access for a bundled entity requires us to pass the
    // bundle type to the access check. As we only have users set a link, we
    // don't currently know the bundle type. Instead, we will rely on checking
    // if the path is valid, which also checks if the current user has access to
    // it.
    if (!$this->pathValidator->isValid($this->configuration['button_link'])) {
      return [
        'message' => [
          '#type' => 'processed_text',
          '#text' => $this->configuration['error']['value'],
          '#format' => $this->configuration['error']['format'],
        ],
      ];
    }

    $output['message'] = [
      '#type' => 'processed_text',
      '#text' => $this->configuration['message']['value'],
      '#format' => $this->configuration['message']['format'],
    ];

    $post_url = Url::fromUserInput($this->configuration['button_link'], [
      'attributes' => [
        'class' => [
          'btn',
          'btn-secondary',
        ],
      ],
    ]);
    $output['link'] = Link::fromTextAndUrl($this->configuration['button_text'], $post_url)->toRenderable();
    $output['link']['#attributes']['class'][] = 'mb-3';

    return $output;
  }

}
