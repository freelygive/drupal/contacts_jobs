<?php

namespace Drupal\contacts_jobs\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to add/edit Job Types.
 */
class JobTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $contacts_job_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $contacts_job_type->label(),
      '#description' => $this->t("Label for the Job type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $contacts_job_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\contacts_jobs\Entity\JobType::load',
      ],
      '#disabled' => !$contacts_job_type->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $contacts_job_type = $this->entity;
    $status = $contacts_job_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Job type.', [
          '%label' => $contacts_job_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Job type.', [
          '%label' => $contacts_job_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($contacts_job_type->toUrl('collection'));
  }

}
