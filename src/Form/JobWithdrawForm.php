<?php

namespace Drupal\contacts_jobs\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Withdraws a job listing.
 *
 * @ingroup contacts_jobs
 */
class JobWithdrawForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $job = $this->getRouteMatch()->getParameter('contacts_job');
    return $this->t('Do you want to withdraw %title?', ['%title' => $job->getTitle()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Withdraw');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs\Entity\Job $entity */
    $entity = $this->entity;

    if ($entity->isPublished()) {
      $entity->setWithdrawn(TRUE);
    }

    $entity->save();

    $this->messenger()->addMessage($this->t('The %title has been withdrawn.', ['%title' => $entity->getTitle()]));
    $form_state->setRedirectUrl($entity->toUrl());
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    // No need to attempt to update fields on the entity.
  }

}
