<?php

namespace Drupal\contacts_jobs\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Marks a job as published.
 *
 * @ingroup contacts_jobs
 */
class JobPublishForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $job = $this->getRouteMatch()->getParameter('contacts_job');
    return t('Do you want to publish %title?', ['%title' => $job->getTitle()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Publish');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs\Entity\Job $entity */
    $entity = $this->entity;
    $entity->setPublishStartTime(time());
    $entity->setWithdrawn(FALSE);
    $entity->save();

    $this->messenger()->addMessage($this->t('The %title has been published.', ['%title' => $entity->getTitle()]));
    $form_state->setRedirectUrl($entity->toUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    // No need to attempt to update fields on the entity.
  }

}
