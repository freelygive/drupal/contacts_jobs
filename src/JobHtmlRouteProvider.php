<?php

namespace Drupal\contacts_jobs;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Job entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class JobHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($post_route = $this->getPostRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.post_form", $post_route);
    }

    if ($post_route = $this->getPostExistingRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.post_existing_form", $post_route);
    }

    if ($update_route = $this->getUpdateFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.update_form", $update_route);
    }

    if ($repost_route = $this->getRepostFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.repost_form", $repost_route);
    }

    if ($withdraw_route = $this->getWithdrawFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.withdraw_form", $withdraw_route);
    }

    if ($publish_route = $this->getPublishFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.publish_form", $publish_route);
    }

    return $collection;
  }

  /**
   * Returns a route for the Post job form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getPostRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('post-form')) {
      $entity_type_id = $entity_type->id();
      $parameters = [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ];

      $route = new Route($entity_type->getLinkTemplate('post-form'));
      // Content entities with bundles are added via a dedicated controller.
      $bundle_entity_type_id = $entity_type->getBundleEntityType();
      // Use the post form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('post')) {
        $operation = 'post';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Post a job',
          'contacts_job_type' => 'job',
          'workflow' => 'post',
        ]);
      $route
        ->setRequirement('_permission', 'post new jobs');
      $parameters[$bundle_entity_type_id] = ['type' => 'entity:' . $bundle_entity_type_id];

      $route
        ->setOption('parameters', $parameters);

      return $route;
    }
  }

  /**
   * Returns a route for the Post job form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getPostExistingRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('post-existing-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('update-form'));
      // Use the update form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('update')) {
        $operation = 'update';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Post job',
          'workflow' => 'post',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getUpdateFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('update-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('update-form'));
      // Use the update form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('update')) {
        $operation = 'update';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Edit job',
          'workflow' => 'edit',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRepostFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('repost-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('repost-form'));
      // Use the repost form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('repost')) {
        $operation = 'repost';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Repost job',
          'workflow' => 'repost',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  /**
   * Returns a route for the Withdraw form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getWithdrawFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('withdraw-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('withdraw-form'));
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.withdraw",
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.withdraw")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  /**
   * Returns a route for the Publish form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getPublishFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('publish-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('publish-form'));
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.publish",
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.publish")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

}
