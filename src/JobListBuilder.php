<?php

namespace Drupal\contacts_jobs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Job entities.
 *
 * @ingroup contacts_jobs
 */
class JobListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Job ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    // Rather than using the normal list, render our view with the filters.
    return views_embed_view('job_list_admin', 'embed');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    $operations['view'] = [
      'title' => $this->t('View'),
      'url' => $this->ensureDestination($entity->toUrl('canonical')),
      // Weight of 11 means it goes below edit (which has weight of 10)
      'weight' => 11,
    ];

    $operations['withdraw'] = [
      'title' => $this->t('Withdraw'),
      'weight' => 12,
      'url' => $this->ensureDestination($entity->toUrl('withdraw-form')),
    ];

    $operations['publish'] = [
      'title' => $this->t('Publish'),
      'weight' => 13,
      'url' => $this->ensureDestination($entity->toUrl('publish-form')),
    ];

    return $operations;
  }

}
