<?php

namespace Drupal\contacts_jobs;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Link;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * View builder handler for Jobs.
 */
class JobViewBuilder extends EntityViewBuilder {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @var static $class */
    $class = parent::createInstance($container, $entity_type);
    $class->currentUser = $container->get('current_user');
    $class->time = $container->get('datetime.time');
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\contacts_jobs\Entity\JobInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $delta => $entity) {

      $bundle = $entity->bundle();
      $display = $displays[$bundle];

      if (isset($build[$delta]['job_salary']) && isset($build[$delta]['job_salary_max']) && !$entity->get('job_salary_max')->isEmpty()) {
        foreach (Element::children($build[$delta]['job_salary_max']) as $item_delta) {
          if (isset($build[$delta]['job_salary'][$item_delta])) {
            $build[$delta]['job_salary'][$item_delta] = ['from' => $build[$delta]['job_salary'][$item_delta]];
            $build[$delta]['job_salary'][$item_delta]['to'] = ['#markup' => ' - '];
          }
          $build[$delta]['job_salary'][$item_delta]['max'] = $build[$delta]['job_salary_max'][$item_delta];
        }
        unset($build[$delta]['job_salary_max']['#access']);
      }

      if ($display->getComponent('links')) {
        $build[$delta]['links'] = [
          '#theme' => 'links__contacts_jobs',
          '#pre_render' => [[Link::class, 'preRenderLinks']],
          '#attributes' => ['class' => ['links', 'inline']],
          '#links' => [],
          '#cache' => [
            'context' => ['user'],
          ],
        ];

        if ($entity->access('view')) {
          $build[$delta]['links']['#links']['view'] = [
            'title' => $this->t('More info'),
            'weight' => 0,
            'url' => $entity->toUrl('canonical'),
          ];
        }
        if ($entity->access('update')) {
          // If published or editing as staff, show the direct edit.
          if ($entity->isPublished() || $entity->getOwnerId() != $this->currentUser->id()) {
            $build[$delta]['links']['#links']['edit'] = [
              'title' => $this->t('Edit'),
              'weight' => 5,
              'url' => $entity->toUrl('update-form'),
            ];
          }
          // If job was ever published, show the re-post list.
          elseif ($entity->getPublishStartTime()) {
            $build[$delta]['links']['#links']['edit'] = [
              'title' => $this->t('Re-post'),
              'weight' => -55,
              'url' => $entity->toUrl('repost-form'),
            ];
          }
          // Otherwise, the job has never been published, so show the post
          // existing link.
          else {
            $build[$delta]['links']['#links']['edit'] = [
              'title' => $this->t('Post'),
              'weight' => -55,
              'url' => $entity->toUrl('post-existing-form'),
            ];
          }
        }
        if ($entity->access('withdraw')) {
          $build[$delta]['links']['#links']['withdraw'] = [
            'title' => $this->t('Withdraw'),
            'weight' => 10,
            'url' => $entity->toUrl('withdraw-form'),
          ];
        }

        usort($build[$delta]['links']['#links'], [
          SortArray::class,
          'sortByWeightElement',
        ]);
      }

      if ($display->getComponent('organisation_logo') && $profile = $this->getOrganisationProfile($entity)) {
        if ($profile->hasField('org_image')) {
          $build[$delta]['organisation_logo'] = $profile->get('org_image')->view([
            'label' => 'hidden',
            'type' => 'image',
            'settings' => [
              'image_style' => $view_mode == 'listing' ? 'thumbnail' : 'medium',
            ],
          ]);
        }
      }

      if ($display->getComponent('organisation_address') && $profile = $this->getOrganisationProfile($entity)) {
        /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem|null $address */
        $address = $profile->get('crm_org_address')->first();
        if ($address) {
          $build['organisation_address'] = $address->view();
        }
      }
    }
  }

  /**
   * Get a profile from the organisation.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The Job entity.
   * @param string $profile
   *   The profile type. Defaults to the organisation profile.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The profile or NULL if there isn't one available.
   */
  protected function getOrganisationProfile(JobInterface $job, string $profile = 'crm_org'): ?ProfileInterface {
    /** @var \Drupal\user\UserInterface|null $organisation */
    $organisation = $job->organisation->entity;
    if (!$organisation) {
      return NULL;
    }

    return $organisation->get("profile_{$profile}")->entity;
  }

}
