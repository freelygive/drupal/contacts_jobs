<?php

namespace Drupal\contacts_jobs_commerce\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Contacts Jobs Commerce Orders subscriber.
 */
class OrderSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.pre_transition' => ['setOrderNumber', 50],
      OrderEvents::ORDER_PAID => 'onPaid',
    ];
  }

  /**
   * Sets the order number to the order ID.
   *
   * Skipped if the order number has already been set.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function setOrderNumber(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() == 'contacts_job' && !$order->getOrderNumber()) {
      /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
      $job = $order->get('contacts_job')->entity;
      $order->setOrderNumber("JOB-{$job->id()}-{$order->id()}");
    }
  }

  /**
   * Increments an order flag each time the paid event gets dispatched.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The event.
   */
  public function onPaid(OrderEvent $event) {
    $order = $event->getOrder();
    if ($order->bundle() != 'contacts_job') {
      return;
    }

    /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
    $job = $order->get('contacts_job')->entity;

    // If the job is already published, extend the publish end by the posting
    // duration.
    // @todo Configure dynamic posting duration.
    if ($job->isPublished()) {
      // If we don't have an end, use now as our baseline.
      $end = $job->getPublishEndTime() ?: $this->time->getRequestTime();
      $end = strtotime('+3 months', $end);
    }
    else {
      $now = $this->time->getRequestTime();
      $job->setPublishStartTime($now);
      $end = strtotime('+3 months', $now);
    }
    $job->setPublishEndTime($end);
    $job->save();
  }

}
