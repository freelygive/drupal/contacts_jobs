<?php

namespace Drupal\contacts_jobs_commerce\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Returns responses for contacts_jobs_commerce routes.
 */
class PaymentReturnController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')->get('contacts_job_commerce')
    );
  }

  /**
   * Redirect appropriately on payment return.
   */
  public function return(OrderInterface $commerce_order, Request $request, string $workflow) {
    $job = $this->getJob($commerce_order);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $commerce_order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . OffsitePaymentGatewayInterface::class);
    }

    try {
      $payment_gateway_plugin->onReturn($commerce_order, $request);
      return $this->redirectComplete($commerce_order, $job);
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
      return $this->redirectError($job, $workflow);
    }
  }

  /**
   * Redirect appropriately on payment cancel.
   */
  public function cancel(OrderInterface $commerce_order, Request $request, string $workflow) {
    $job = $this->getJob($commerce_order);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $commerce_order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . OffsitePaymentGatewayInterface::class);
    }

    $payment_gateway_plugin->onCancel($commerce_order, $request);
    return $this->redirectError($job, $workflow);
  }

  /**
   * Get the job from the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The job.
   */
  protected function getJob(OrderInterface $order): JobInterface {
    if ($order->bundle() != 'contacts_job') {
      throw new BadRequestHttpException('Given order is not a valid for this route.');
    }

    /** @var \Drupal\contacts_jobs\Entity\JobInterface|null $job */
    $job = $order->get('contacts_job')->entity;
    if (!$job) {
      throw new BadRequestHttpException('Given order is not a valid for this route.');
    }

    return $job;
  }

  /**
   * Redirect to the job on completion.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order we are processing.
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job the payment was for.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirection response.
   */
  protected function redirectComplete(OrderInterface $order, JobInterface $job) {
    // Mark the order as placed.
    $state = $order->getState();
    if (in_array('place', array_keys($state->getTransitions()))) {
      $state->applyTransitionById('place');
      $order->save();
    }

    return $this->redirect('entity.contacts_job.canonical', [
      'contacts_job' => $job->id(),
    ]);
  }

  /**
   * Redirect back a step on error.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job the payment was for.
   * @param string $workflow
   *   The workflow we were in.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirection response.
   */
  protected function redirectError(JobInterface $job, string $workflow) {
    return $this->redirect('contacts_jobs_commerce.contacts_job.payment', [
      'contacts_job' => $job->id(),
      'workflow' => $workflow,
    ]);
  }

}
